import menuItemsData from 'data/menu';
import routes from 'data/routes';

import Router from 'router';
import MainMenu from 'menu';

const mainMenu = new MainMenu(menuItemsData);
const router = new Router(routes);

// Tippy.JS config
tippy.setDefaults({
    arrow: true,
    animation: 'scale',
    placement: 'top'
});
