export default class Vault {
    container = null;
    slots = [];

    constructor(element, items) {
        this.container = document.querySelector(element);
        this._items = items || [];

        this._generateSlots();
        this._drawItems();
        this._dropSlots();
    }

    _drawItems() {
        this._items.forEach(({name, image, slot, x, y}) => {
            const element = this._generateItemElement({name, image, slot, x, y});
            this.container.append(element);
        });
    }

    _dropSlots() {
        this.slots.forEach(slot => {
            slot.addEventListener('dragover', this._dragOverSlot)
            slot.addEventListener('dragleave', this._dragLeftSlot)
        });
    }

    _dragOverSlot = ({target}) => {
        target.style.backgroundImage = 'url(\'/assets/images/slot_taken.jpg\')';
    }

    _dragLeftSlot = ({target}) => {
        target.style.backgroundImage = '';
    }

    _generateItemElement({name, image, slot, x, y}) {
        const img = document.createElement('img')
        img.setAttribute('src', image);
        img.setAttribute('alt', name);

        const element = document.createElement('div')
        element.setAttribute('class', 'item');
        element.setAttribute('draggable', 'true');
        element.dataset.slot = slot;
        element.dataset.x = x;
        element.dataset.y = y;
        element.append(img);

        return element;
    }

    _generateSlots() {
        for (let slot = 0; slot < 120; slot++) {
            const element = document.createElement('div');
            element.setAttribute('class', 'slot');
            element.dataset.slot = slot;
            this.container.append(element);
            this.slots.push(element);
        }
    }
}
