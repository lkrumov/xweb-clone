export default class MainMenu {
    static itemSeparator = 'fas fa-angle-double-right';

    mainMenuElement;
    subMenuElement;
    items = [];

    constructor(items) {
        this.mainMenuElement = document.querySelector('#main-menu');
        this.subMenuElement = document.querySelector('#sub-menu');
        this.items = items;

        if (!this.mainMenuElement) {
            throw new Error(`[MainMenu] Couldn't find main menu element`);
        }

        if (!this.subMenuElement) {
            throw new Error(`[MainMenu] Couldn't find sub menu element`);
        }

        this.draw();
    }

    draw() {
        // Main Menu should be drawn only one time!
        if (this._drawn) throw new Error(`[MainMenu] Attempt to re-draw MainMenu`);

        this._reverse(this.items).forEach(({title, submenu, ...attributes}) => {
            // Create & append element to the main menu
            const element = this._createMenuElement(title, attributes);
            this.mainMenuElement.prepend(element);

            if (submenu) {
                // Adds mouse over event listener to the main menu's button
                element.addEventListener('mouseover', () => {
                    this.subMenuElement.innerHTML = '';

                    // Displays the sub menu options
                    this._reverse(submenu).forEach(({title, submenu, ...attributes}) => {
                        const element = this._createMenuElement(title, attributes);
                        this.subMenuElement.prepend(this.separator, element);
                    });
                });
            }
        });

        this._drawn = true;
    }

    get separator() {
        const classList = MainMenu.itemSeparator.split(' ');
        const separator = document.createElement('i');
        separator.classList.add(...classList);
        return separator;
    }

    _createMenuElement = (title, attributes) => {
        const itemElement = document.createElement('a');
        const setAttribute = key => itemElement.setAttribute(key, attributes[key]);
        itemElement.innerText = title;
        Object.keys(attributes).forEach(setAttribute);
        return itemElement;
    }

    _reverse(collection) {
        return collection.slice().reverse();
    }
}
