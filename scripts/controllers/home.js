import EventTimer from 'event-timer';
import TabSystem from 'tabs';

export default class HomeController {
    tabs = [];
    timers = [];

    load() {
        this.tabs = new TabSystem('#home-tabs');
        this.timers.push(
            new EventTimer('#blood-castle-timer', ['00:00', '1:00', '3:00', '6:00', '11:00', '16:00', '19:00', '22:00'])
        );
    }

    unload() {
        this.timers.forEach(timer => timer.stop());
    }
}
