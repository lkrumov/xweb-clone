import TabSystem from 'tabs';

export default class CharacterController {
    load(params) {
        this.panel = document.querySelector('.character-panel');
        this.characterName = this.panel.querySelector('#character-name');
        this.characterImg = this.panel.querySelector('#character-image');

        // Load Character data
        return this._getCharacterData(params.name).then(this._displayCharacterData);
    }

    _displayCharacterData = (character) => {
        console.log(character);

        this.characterName.innerText = character.Name;
        this.characterImg.setAttribute('src', character.ClassImage);
        this.characterImg.setAttribute('alt', character.Class);
        this.tabs = new TabSystem('#character-panel-tabs');
    }

    _getCharacterData(name) {
        return Promise.resolve({
            Name: name,
            Guild: {
                Name: 'ADMINS',
                Position: 'Leader',
                Members: 5
            },
            Map: {
                Name: 'Lorencia',
                Coordinates: [120, 120]
            },
            Class: 'Dimension Master',
            ClassImage: '/assets/classes/bloody-summoner-character.png',
            cLevel: 400,
            Resets: 21,
            GrandResets: 2,
            LevelUpPoint: 1000,
            Strength: 9800,
            Dexterity: 10063,
            Vitality: 5000,
            Energy: 12000
        });
    }
}
