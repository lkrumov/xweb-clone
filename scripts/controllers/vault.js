import Vault from 'vault';
// import dragula from 'libraries/dragula';

// TODO: Work over drag and drop!
export default class VaultController {
    load() {
        this._userVault = new Vault('section.vault.user', [
            {
                name: 'Great Dragon Armor',
                image: '/assets/items/00158000.gif',
                slot: 0, x: 2, y: 3
            },
            {
                name: 'Wings of Elf',
                image: '/assets/items/10838000.gif',
                slot: 32, x: 4, y: 3
            },
            {
                name: 'Box of Kundun +2',
                image: '/assets/items/00cb800009.gif',
                slot: 10, x: 1, y: 1
            }
        ]);
        this._webVault = new Vault('section.vault.web', [{
            name: 'Box of Kundun +2',
            image: '/assets/items/00cb800009.gif',
            slot: 100, x: 1, y: 1
        }]);

        // this._dragAndDrop();
    }

    // _dragAndDrop() {
    //     this._drake = dragula([this._userVault.container, this._webVault.container], {
    //         accepts: '.items',
    //         moves: '.items'
    //     });
    // }
}
