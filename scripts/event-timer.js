// TODO: Deal with timezones - moment.js
// TODO: Refactor the crap out of it!
export default class EventTimer {
    eventTimes = [];
    elementId;
    element;

    constructor(elementId, eventTimes) {
        this.elementId = elementId;
        this.element = document.querySelector(elementId);
        this.eventTimes = eventTimes.map(this._transformTimeString);

        if (!this.element) {
            throw new Error(`[Timer] Couldn't find element with id ${elementId}`)
        }

        this.start();
    }

    start() {
        this._updateTimer();
        this._interval = setInterval(this._updateTimer, 500);
    }

    stop() {
        clearInterval(this._interval);
    }

    _transformTimeString = time => {
        const [hours, minutes] = time.split(':');
        const date = new Date();
        date.setHours(hours);
        date.setMinutes(hours);
        return date;
    }

    _updateTimer = () => {
        this.element.innerText = this._getEventTime();
    }

    _getEventTime() {
        let closestTime = this._getClosestEventTime();

        if (!closestTime) {
            closestTime = this._getClosestEventTime();
        };

        if (closestTime.difference < 0) {
            return 'IN PROGRESS';
        }

        if (closestTime.difference <= 120) {
            return 'OPEN';
        }

        let seconds = closestTime.difference;
        const hours = Math.floor(seconds / 60 / 60);
        seconds -= hours * 60 * 60;
        const minutes = Math.floor(seconds / 60);
        seconds -= minutes * 60;

        if (hours && minutes && seconds) {
            return `${this._padZero(hours)}:${this._padZero(minutes)}:${this._padZero(seconds)}`;
        } else if (minutes && seconds) {
            return `${this._padZero(minutes)}:${this._padZero(seconds)}`;
        }

        return `00:${this._padZero(seconds)}`;
    }

    _getClosestEventTime() {
        const currentDate = new Date();
        return this.eventTimes.filter(date => {
            const diff = this._getEventTimeDifference(currentDate, date);

            if (diff > 0 || diff > -120) {
                return true;
            }

            // Set schedule for next day, if not set already
            if (date.getDate() === currentDate.getDate()) {
                date.setDate(date.getDate() + 1);
                this._getEventTimeDifference(currentDate, date);
            }

            return false;
        })[0]
    }

    _getEventTimeDifference(currentDate, date) {
        const diff = Math.ceil((date - currentDate) / 1000);
        date.difference = diff;
        return diff;
    }

    _padZero(number) {
        return (number + '').padStart(2, '0');
    }
}
