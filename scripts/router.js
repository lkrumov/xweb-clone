export default class Router {
    static loadingImage = '/assets/images/loading.gif'
    static notFoundModule = 'modules/404';
    static prefix = '#/';

    currentController;
    currentRoute;
    container;
    routes;

    constructor(routes) {
        this.routes = routes;
        this.container = document.querySelector('main');

        if (!this.container) {
            throw new Error(`[Router] Couldn't find main element`);
        }

        // Initiate router handling
        window.addEventListener('load', this._handleRoute);
        window.addEventListener('popstate', this._handleRoute);

        // Initiate loading state, before the document is loaded
        this._startLoading();
    }

    loadModule = (module) => {
        this._startLoading();

        return fetch(module + '.html').then(response => {
                    // TODO: Handle response codes: 404, 403, 401, etc...
                    if (!response.ok) {
                        throw new Error(`[Router] Module <${module}> not found!`);
                    }

                    return response.text();
                }).then(content => {
                    // This will remove the nodes from
                    this.container.innerHTML = content;
                }).catch(this._notFound);
    }

    get overlay() {
        if (!this._overlay) {
            this._overlay = document.createElement('div');
            this._overlay.setAttribute('class', 'overlay');
        }

        return this._overlay;
    }

    get loader() {
        if (!this._loader) {
            this._loader = document.createElement('img');
            this._loader.setAttribute('src', Router.loadingImage);
            this._loader.setAttribute('alt', 'Loading...');
            this._loader.setAttribute('class', 'loader');

        }

        return this._loader;
    }

    _startLoading = () => {
        this.container.append(this.overlay, this.loader);
    }

    _stopLoading = () => {
        this.overlay.remove();
        this.loader.remove();
    }

    _handleRoute = () => {
        let hashData = {};

        const hash = window.location.hash;
        const prefix = Router.prefix;
        const route = this.routes.filter(({route}) => {
            const currentRoute = (prefix + route); // TODO: Handle aliases (e.g. ['', 'home'])
            const currentHash = (hash || prefix);
            const match = route => currentHash === (prefix + route);
            const exactMatch = !Array.isArray(route) ? match(route) : route.filter(match).length > 0;

            // We have exact match - hurray!
            if (exactMatch) return true;

            // Router is not found by the simple search method - try RegEx
            const regexMatches = currentRoute.match(/\{(\w+):(.*?)}/g);

            // If there are no matches - no RegEx
            if (!regexMatches) return false;

            // Prepare RexEx-ed routes
            const routeParts = currentRoute.split('/');
            const hashParts = currentHash.split('/');

            // If the length is not matching, it's not our route for sure!
            if (routeParts.length !== hashParts.length) return false;

            const mappedRouteParts = routeParts.map((routePart, index) => {
                // Try to match the route part with the extracted RegEx-s
                const [regexPart] = regexMatches.filter(regexMatch => regexMatch === routePart);

                // No RegEx matching the route part
                if (!regexPart) return routePart;

                // Route part contains RegEx - extract it
                const [param, name, regex] = regexPart.match(/^\{(\w+):(.*?)\}$/) || [];
                return {name, regex};
            });

            const {regexMatch, params} = this._matchRouteParts(hashParts, mappedRouteParts);

            // If we've found RegEx route - assign potential params
            if (regexMatch) hashData = params;

            return regexMatch
        })[0];

        // At this point some router will be loaded for sure - start the unload
        // Maybe handle promises here?
        if (this.currentController && this.currentController.unload) {
            this.currentController.unload();
        }

        if (!route) {
            this._notFound();
            throw new Error(`[Router] Route <${hash}> not found!`);
        }

        this.loadModule(route.module).then(() => {
            this.currentRoute = route;
            this.currentController = route.controller ? new route.controller : null;

            if (this.currentController && this.currentController.load) {
                const loading = this.currentController.load(hashData);

                if (loading && loading.finally) {
                    this._startLoading();
                    return loading.finally(this._stopLoading);
                }
            }
        });
    }

    _matchRouteParts(hashParts, routeParts) {
        const params = {};
        let regexMatch = true;

        routeParts.forEach((part, index) => {
            // Not matching, no need to try the rest
            if (!regexMatch) return;

            const hashPart = hashParts[index];
            let matchingString = part.regex || part;

            // Appends $ at the beginning of the matching string, if not present
            if (!matchingString.match(/\$$/)) matchingString = matchingString + '$';
            // Appends ^ at the end of the matching string, if not present
            if (!matchingString.match(/^\^/)) matchingString = '^' + matchingString;

            const matcher = new RegExp(matchingString);

            // If any part of the hash is not matching - no match
            if (!matcher.test(hashPart)) {
                regexMatch = false;
            }
            // If the parameter has name - include it in the params
            else if (part.name) {
                params[part.name] = hashPart;
            }
        });

        return {regexMatch, params};
    }

    _notFound = () => {
        return this.loadModule(Router.notFoundModule);
    }
}
