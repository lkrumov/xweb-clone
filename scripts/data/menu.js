const item = (title, href, options) => ({title, href, ...options});

export const routes = [
    item('home', '#/home'),
    item('account', '#/account', {
        submenu: [
            item('characters', '#/account/characters'),
            item('change password', '#/account/change-password'),
            item('inbox', '#/account/inbox'),
            item('web vault', '#/account/vault')
        ]
    }),
    item('rates', '#/rates'),
    item('downloads', '#/downloads'),
    item('rankings', '#/rankings', {
        submenu: [
            item('characters', '#/rankings/characters'),
            item('killers', '#/rankings/killers'),
            item('voters', '#/rankings/voters'),
            item('guilds', '#/rankings/guilds'),
            item('online', '#/rankings/online'),
            item('banned', '#/rankings/banned'),
            item('admins', '#/rankings/admins'),
            item('devil square', '#/rankings/devil-square'),
            item('blood castle', '#/rankings/blood-castle')
        ]
    }),
    item('quests', '#/quests', {
        submenu: [
            item('quest list', '#/quests/list'),
            item('my quest history', '#/quests/history'),
            item('quest ladder', '#/quests/ladder')
        ]
    }),
    item('market', '#/market', {
        submenu: [
            item('search', '#/market/search'),
            item('my market', '#/market/my'),
            item('sell item', '#/market/sell')
        ]
    }),
    item('user gallery', '#/gallery'),
    item('webshop', '#/webshop', {
        submenu: [
            item('buy items', '#/webshop/items')
        ]
    }),
    item('forums', '#/forums')
];

export default routes;
