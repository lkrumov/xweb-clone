import HomeController from '../controllers/home';
import VaultController from '../controllers/vault';
import InboxController from '../controllers/inbox';
import CharacterController from '../controllers/character';

export const routes = [
    {route: ['', 'home'], module: 'modules/home', controller: HomeController},
    {route: 'account', module: 'modules/account/index'},
    {route: 'account/characters', module: 'modules/account/characters'},
    {route: 'account/character/{name:[A-Za-z_0-9]+}', module: 'modules/account/character-panel', controller: CharacterController},
    {route: 'account/vault', module: 'modules/account/vault', controller: VaultController},
    {route: 'account/inbox', module: 'modules/account/inbox', controller: InboxController},
    {route: 'rankings', module: 'modules/rankings/index'},
    {route: 'rankings/admins', module: 'modules/rankings/admins'},
    {route: 'rankings/banned', module: 'modules/rankings/banned'},
    {route: 'rankings/blood-castle', module: 'modules/rankings/blood-castle'},
    {route: 'rankings/characters', module: 'modules/rankings/characters'},
    {route: 'rankings/devil-square', module: 'modules/rankings/devil-square'},
    {route: 'rankings/guilds', module: 'modules/rankings/guilds'},
    {route: 'rankings/killers', module: 'modules/rankings/killers'},
    {route: 'rankings/online', module: 'modules/rankings/online'},
    {route: 'rankings/voters', module: 'modules/rankings/voters'},
    {route: 'downloads', module: 'modules/downloads'},
    {route: 'quests/list', module: 'modules/quests/quests'},
    {route: 'quests/history', module: 'modules/quests/history'},
    {route: 'quests/ladder', module: 'modules/quests/ladder'},
    {route: 'quests/{name:[A-Za-z_0-9]+}', module: 'modules/quests/list'},
    {route: 'quests/{name:[A-Za-z_0-9]+}/{id:\\d+}', module: 'modules/quests/single'},
    {route: 'character/{name:[A-Za-z_0-9]+}', module: 'modules/character'},
    {route: 'guild/{name:[A-Za-z_0-9]+}', module: 'modules/guild'},
    {route: 'market', module: 'modules/market/index'},
    {route: 'market/my', module: 'modules/market/my-items'},
    {route: 'market/sell', module: 'modules/market/sell'},
    {route: 'market/search', module: 'modules/market/search'},
    {route: 'market/search/{query:[A-Za-z\\\'\\+0-9]+}', module: 'modules/market/search'},
    {route: 'webshop', module: 'modules/webshop/index'},
    {route: 'webshop/items', module: 'modules/webshop/categories'},
    {route: 'webshop/items/{id:\\\d+}', module: 'modules/webshop/category'},
    {route: 'webshop/buy/{id:\\\d+}', module: 'modules/webshop/buy'},
];

export default routes;
