
export default class TabSystem {
    static selected = 'selected';

    selector;
    wrapper;
    content;
    tabs;

    constructor(selector) {
        this.selector = selector;
        this.wrapper = document.querySelector(selector);
        this.tabs = document.querySelectorAll(this.tabsSelector);
        this.tabs = Array.from(this.tabs); // Convert NodeList to Array
        this.content = document.querySelectorAll(this.contentSelector);
        this.content = Array.from(this.content); // Convert NodeList to Array

        if (!this.wrapper) {
            throw new Error(`[TabSystem] (${selector}) Couldn't find wrapper element`);
        }

        if (!this.tabs || !this.content) {
            throw new Error(`[TabSystem] (${selector}) Couldn't find content or tabs elements`);
        }

        if (this.tabs.length !== this.content.length) {
            throw new Error(`[TabSystem] (${selector}) Tabs and content count doesn't match!`);
        }

        this.tabulate();
    }

    tabulate() {
        this.tabs.forEach((tab, index) => {
            const content = this.content[index];

            if (!content) {
                throw new Error(`[TabSystem] (${this.selector}) Content element not found!`);
            }

            tab.addEventListener('click', event => {
                // Stop event from bubbling
                event.stopPropagation();
                event.preventDefault();

                // Display the tab
                this._deselect();
                this._addSelectedClass(content, tab);
            })

            if (tab.classList.contains(TabSystem.selected)) {
                this._addSelectedClass(content);
            }
        });
    }

    get tabsSelector() {
        return this.selector + ' > .pills > a';
    }

    get contentSelector() {
        return this.selector + ' > .content > div';
    }

    _deselect() {
        this.content.forEach(this._removeSelectedClass);
        this.tabs.forEach(this._removeSelectedClass);
    }

    _removeSelectedClass = (node) => {
        node.classList.remove(TabSystem.selected);
    }

    _addSelectedClass = (...nodes) => {
        nodes.forEach(node => node.classList.add(TabSystem.selected));
    }
}
