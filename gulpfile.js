const gulp = require('gulp');
const sass = require('gulp-sass');
const babel = require('gulp-babel');
const merge = require('gulp-sequence');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const minify = require('gulp-clean-css');
const include = require('gulp-file-include');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();

// Libraries
const libraries = [
    'node_modules/requirejs/require.js',
    'node_modules/tippy.js/dist/tippy.all.js',
    'node_modules/dragula/dist/dragula.js'
];

// File paths
const root = './build';
const scripts = 'scripts/**/*.js';
const scriptsBasepath = 'scripts';
const scriptsDestination = root + '/scripts';
const librariesDestination = scriptsDestination + '/libraries';
const scss = 'scss/*.scss'; // This will compile only the SCSS files in the root directory!
const scssWatch = 'scss/**/*.scss';
const scssDestination = root + '/assets';
const htmlIndex = 'templates/index.html';
const html = [
    'templates/**/*.html',
    '!templates/components/*',
    '!templates/modules/**/partials/*',
    '!templates/*.html'
];
const htmlWatch = 'templates/**/*.html';
const htmlBasepath = 'templates';
const htmlDestination = root;
const assets = [
    'assets/**/*',
    '!assets/**/*.psd'
];
const assetsDestination = root + '/assets';

// Process JS files
gulp.task('scripts', function () {
    return gulp.src(scripts)
            .pipe(include({basepath: scriptsBasepath}).on('error', console.error))
            .pipe(sourcemaps.init())
            .pipe(babel({
                presets: ['es2015', 'stage-1'],
                plugins: [
    				'babel-plugin-transform-es2015-modules-commonjs',
    				'transform-es2015-modules-amd'
    			]
            }).on('error', console.error))
            // .pipe(concat('scripts.js')) // Don't bundle temporary!
            // .pipe(uglify())
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(scriptsDestination));
});

// Copy libraries
gulp.task('libraries', function () {
    return gulp.src(libraries).pipe(gulp.dest(librariesDestination));
});

// Copy HTML files
gulp.task('html', function () {
    const copyAndInclude = src => {
        return src.pipe(include({basepath: htmlBasepath}).on('error', console.error))
                .pipe(gulp.dest(htmlDestination));
    };

    return merge(
        copyAndInclude(gulp.src(html)),
        copyAndInclude(gulp.src(htmlIndex))
    );
});

// Copy Assets files
gulp.task('assets', function () {
    return gulp.src(assets).pipe(gulp.dest(assetsDestination));
});

// Compile with gulp-ruby-sass + source maps
gulp.task('sass', function () {
    return gulp.src(scss)
            .pipe(sourcemaps.init())
            .pipe(sass().on('error', sass.logError))
            .pipe(minify().on('error', console.error))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(scssDestination))
            .pipe(browserSync.stream());
});

// Serve task - builds the files and serves the content from the root folder
gulp.task('serve', ['libraries', 'scripts', 'sass', 'html', 'assets'], function () {
    browserSync.init({
        port: 4343,
        ghostMode: false,
        open: false,
        server: {
            baseDir: root
        }
    });
});

// Watch task - runs the serve task and listens for changes
gulp.task('watch', ['serve'], function () {
    gulp.watch(scripts, ['scripts']).on('change', () => setTimeout(browserSync.reload, 250));
    gulp.watch(htmlWatch, ['html']).on('change', browserSync.reload);
    gulp.watch(assets, ['assets']).on('change', browserSync.reload);
    gulp.watch(scssWatch, ['sass']);
});

// Default task for serving the template
gulp.task('default', ['watch']);
